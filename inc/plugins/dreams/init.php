<?php
// Get Dashboard
require_once DREAMS_DIR .  '/dashboard/plugins-dashdreams.php';


//Common Metas
add_filter('the_content', 'add_image_class');

// add_filter('widget_text','php_execute',100);
// add_filter( 'widget_title', 'remove_widget_title' );
add_filter('upload_mimes', 'cc_mime_types');

//Add Attr
// add_filter( 'wp_get_attachment_image_attributes', 'wpdocs_filter_gallery_img_atts', 10, 2 );

//Commons General
add_action( 'admin_enqueue_scripts', 'dreams_dashboard_css', 99 );
add_action('login_head', 'dreams_login_css');
add_action( 'admin_enqueue_scripts', 'dreams_dashboard_js', 100 );
add_action( 'login_headerurl', 'dreams_login_url',99);
add_action('admin_footer_text', 'dreams_footer_admin');
