<?php
namespace MBUP\Shortcodes;

use RWMB_Helpers_Array as ArrayHelper;
use MBUP\Forms\Login as Form;
use MBUP\User;
use MBUP\Appearance;

class Login extends Base {
	/**
	 * Shortcode type.
	 *
	 * @var string
	 */
	protected $type = 'login';

	/**
	 * Get the form.
	 *
	 * @param array $args Form configuration.
	 *
	 * @return Form Form object.
	 */
	protected function get_form( $args ) {
		$args = (array) $args;

		// Compatible with old shortcode attributes.
		ArrayHelper::change_key( $args, 'remember', 'label_remember' );
		ArrayHelper::change_key( $args, 'lost_pass', 'label_lost_password' );
		ArrayHelper::change_key( $args, 'submit_button', 'label_submit' );

		$args = shortcode_atts( array(
			'redirect'            => '',
			'form_id'             => 'login-form',

			// Appearance options.
			'label_username'      => __( 'Username or Email Address', 'mb-user-profile' ),
			'label_password'      => __( 'Password', 'mb-user-profile' ),
			'label_remember'      => __( 'Remember Me', 'mb-user-profile' ),
			'label_lost_password' => __( 'Lost Password?', 'mb-user-profile' ),
			'label_submit'        => __( 'Log In', 'mb-user-profile' ),

			'id_username'         => 'user_login',
			'id_password'         => 'user_pass',
			'id_remember'         => 'rememberme',
			'id_submit'           => 'submit',

			'remember'            => true,

			'value_username'      => '',
			'value_remember'      => false,

			'confirmation'        => __( 'You are now logged in.', 'mb-user-profile' ),

			'password_strength'   => 'false',
		), $args );

		// Apply changes to appearance.
		$base_meta_box = rwmb_get_registry( 'meta_box' )->get( 'rwmb-user-login' );
		$appearance = new Appearance( $base_meta_box );

		$appearance->set( 'username.name', $args['label_username'] );
		$appearance->set( 'username.id', $args['id_username'] );

		$appearance->set( 'password.name', $args['label_password'] );
		$appearance->set( 'password.id', $args['id_password'] );

		$meta_boxes = array( $base_meta_box );

		$user = new User( $args );

		return new Form( $meta_boxes, $user, $args );
	}
}
