<?php
  /*
    Plugin Name: Home Video Sect
  */

  
add_action('widgets_init', 'pinno_load_home_video_sect_widget');

function pinno_load_home_video_sect_widget() {
  register_widget('pinno_home_video_sect_widget');
}

class pinno_home_video_sect_widget extends WP_Widget { 
  // class constructor
	public function __construct() {
      $widget_ops = array( 
          'classname' => 'pinno_home_video_sect_widget',
          'description' => 'A widget that displays the destacate posts',
      );
      parent::__construct( 'pinno_home_video_sect_widget', 'Iggy forbesCol Home Video Section Widget', $widget_ops);
    }
    
    public function widget( $args, $instance ) { 
        global $module2_video_id;
        //Loop Arguments
        $module_two_video_args = array(
         'no_found_rows' => true,
         'update_post_meta_cache' => false,
         'update_post_term_cache' => false,
         'posts_per_page' => 1,
         'post_type' => 'any',
         'ignore_sticky_posts' => true,
         'tax_query' => array(
           array(
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array(
              'post-format-video'
            ),
            'operator' => 'IN'
           )
         )
        );

        //Query
        $module_two_video = new WP_Query($module_two_video_args); 

       
        //Begin Loop
        if ($module_two_video->have_posts()) : while ($module_two_video->have_posts()) : $module_two_video->the_post(); 
         global $post;
        ?>

        <?php $module2_video_id = $post->ID; ?>
        
        <?php
        endwhile; endif;
    ?>

      <section id="section-3" class="mainsection mainsection--video">
        <div id="bg-sect" class="featured__bg-image featured__bg-image--current"></div>
        <div class="channel--header">
              <h2 style="color:white; z-index:99;">  
                 Forbes TV  
              </h2> 
            </div>
        <div class="featured__bg-pattern"></div>
        <div class="f4_mainsection__wrap">
          
        
          
          <?php  
            $module_six_video_args = array(
              'posts_per_page' => '5',
              'post_type' => 'any',
              'post__not_in' => array($module2_video_id),
              'tax_query' => array(
                array(
                  'taxonomy' => 'post_format',
                  'field' => 'slug',
                  'terms' => array(
                    'post-format-video'
                  ),
                  'operator' => 'IN'
                )
              )
            );
          ?>
          <div class="video-wrap">
            
          
          
          <div id="videoGal" class="glide videos-gal">
            <button class="arrow-btn arrow--left">
            <svg class="fs-icon fs-icon--arrow-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.5 10h16v2h-16z"></path><path transform="rotate(-45.001 4.5 8.877)" d="M.5 7.9h8v2h-8z"></path><path transform="rotate(45.001 4.5 13.124)" d="M.5 12.1h8v2h-8z"></path></svg>
            </button>
            
            <button class="arrow-btn arrow--right">  
            <svg class="fs-icon fs-icon--arrow-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path transform="rotate(-180 8.964 11)" d="M1 10h16v2H1z"></path><path transform="rotate(134.999 14.965 13.124)" d="M11 12.1h8v2h-8z"></path><path transform="rotate(-134.999 14.965 8.877)" d="M11 7.9h8v2h-8z"></path></svg>
            </button>
            <div class="glide__track" data-glide-el="track">
              
              <ul class="glide__slides">
                <?php
                  global $prefix;
                  $video_url =  get_post_meta($post->ID, "{$prefix}videos_oembed", true);
                  $video_author = get_the_author_meta('display_name', $post->post_author);
                  $video_image_url = get_the_post_thumbnail_url($post->ID, 'dreams-640x360');
                  $video_permalink = get_post_permalink($post->ID);
                  $video_authorlink = get_author_posts_url($post->post_author);
                ?>
                
                <?php
                  $module_six_video =  get_posts($module_six_video_args);

                  $length = count($module_six_video);
                  var_dump($length);
                  /* if ($module_six_video->have_posts()) : while ($module_six_video->have_posts()) : $module_six_video->the_post(); */
                ?>
                  
                  <li class="glide__slide video_list_preview">
                    <figure class="video_list_youtube-wrapper"  data-except="<?php echo $post->post_excerpt; ?>" data-title="<?php echo $post->post_title; ?>" data-author = "<?php echo $video_author; ?>" data-url = "<?php echo $video_url; ?>" data-image= "<?php echo $video_image_url; ?>"data-permalink = "<?php echo $video_permalink; ?>" data-authorlink = "<?php echo $video_authorlink; ?>">
                      <?php echo get_the_post_thumbnail($post->ID, 'dreams-640x360', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover'));?>
                      <button type="button" class="play-button play-button--list-videos"></button>
                    </figure>
                    
                    <h3 class="video_list_title video_list_title--big"><a class="f4_module_link--white" href="<?php echo $video_permalink; ?>" title="<?php echo $post->post_title; ?>"><?php echo $post->post_title; ?></a></h3>
                      
                      
                      <span class="video_list_author">Por <a class="f4_module_link--white" href="<?php echo $video_authorlink; ?>"> <?php echo $video_author; ?> </a> </span>
                  </li>
                <?php //endwhile; endif; ?>
                <?php wp_reset_query(); ?>
              </ul> <!-- glide__slides //-->
            </div> <!-- glide__track //-->

          </div> <!-- glide //-->

          </div> <!-- video-wrap //-->
        </div>
      </section>
    <?php     
    } 

    	// save options
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['vidgall'] = strip_tags( $new_instance['vidgall'] );

		return $instance;
    }

    public function form( $instance ) {

		/* Set up some default widget settings. */
		 $defaults = array( 'title' => 'Title' );
		 $instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 
		 <!-- Widget Title: Text Input -->
		 <p>
		   <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
		   <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
		 </p>
		   
			   <!-- Video/Gallery -->
		 <p>
		   <label for="<?php echo $this->get_field_id('vidgall'); ?>">Display Videos or Galleries:</label>
		   <select id="<?php echo $this->get_field_id('vidgall'); ?>" name="<?php echo $this->get_field_name('vidgall'); ?>" style="width:100%;">
			   <option value='videos' <?php if ('videos' == $instance['vidgall']) echo 'selected="selected"'; ?>>
                 Videos
               </option>
			   <option value='galleries' <?php if ('galleries' == $instance['vidgall']) echo 'selected="selected"'; ?>>Galleries</option>
		   </select>
		 </p>
	 <?php }
     

}