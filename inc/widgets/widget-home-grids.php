<?php
/**
 * Plugin Name: Home Grid Widget
 */

add_action( 'widgets_init', 'pinno_home_grid_load_widgets' );

function pinno_home_grid_load_widgets() {
	register_widget( 'pinno_home_grid_widget' );
}

class pinno_home_grid_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'pinno_home_grid_widget', 'description' => esc_html__('A widget that displays Grid Sectional and an optional 300x250 ad.', 'iggy-type-0') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'pinno_home_grid_widget' );

		/* Create the widget. */
		parent::__construct( 'pinno_home_grid_widget', esc_html__('IT0 - Home Grid Widget', 'iggy-type-0'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		global $post;
		$title = apply_filters('widget_title', $instance['title'] );
		$tagcat = $instance['tagcat'];
		$enterslug = $instance['enterslug'];
		$sidebarpos = $instance['sidebarpos'];
		$code = $instance['code'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		?>
<section class="channel channel--layout__left">

    <div class="pinno-widget-home-head">
        <h4 class="pinno-widget-home-title"><span class="pinno-widget-home-title"><a href="/<?php echo ($enterslug ? $enterslug : ''); ?>/"><?php echo ($title ? $title : ''); ?> </a></span></h4>
    </div>

    <?php

    if ($tagcat == 'tag') {
        $module_life_first = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'tag' => $enterslug,
            'order' => 'DESC',
            'posts_per_page' => 1,
            'ignore_sticky_posts' => true,
        );

        $module_life_second = array(

            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'tag' => $enterslug,
            'order' => 'DESC',
            'posts_per_page' => 2,
            'ignore_sticky_posts' => true,
            'offset' => 1
        );

        $module_life_third = array(

            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'tag' => $enterslug,
            'order' => 'DESC',
            'posts_per_page' => 4,
            'ignore_sticky_posts' => true,
            'offset' => 3
        );

    } else {

        $module_life_first = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'category_name' => $enterslug,
            'order' => 'DESC',
            'posts_per_page' => 1,
            'ignore_sticky_posts' => true,
        );

        $module_life_second = array(

            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'category_name' => $enterslug,
            'order' => 'DESC',
            'posts_per_page' => 2,
            'ignore_sticky_posts' => true,
            'offset' => 1
        );

        $module_life_third = array(

            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'category_name' => $enterslug,
            'order' => 'DESC',
            'posts_per_page' => 4,
            'ignore_sticky_posts' => true,
            'offset' => 3
        );

    }

        

        $module_life_loop_one = new WP_Query($module_life_first); 
        $module_life_loop_two = new WP_Query($module_life_second); 
        $module_life_loop_three = new WP_Query($module_life_third); 

    ?>

    <div class="channel--content">

        <div class="channel--content-first">
        
        <?php
        // First Main Article Loop ($module_life_loop_one)
        if ($module_life_loop_one->have_posts()) :
            while ($module_life_loop_one->have_posts()) :
                $module_life_loop_one->the_post(); ?>  

            <article class="channel--article-style channel--content-first__main">
                <figure>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail('large'); ?>
                </a>
                </figure>
                <h3 class="channel--article-style_title channel--article-style_title__big">                
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h3>
                <div class="channel--article-style__metas">
                <span>por <i><?php echo get_the_author_meta('display_name'); ?></i></span>
                </div>
            </article>

            <?php endwhile; endif;
            wp_reset_postdata(); 
            // \\End First Article Loop ($module_life_loop_one)
            ?>
            


            <div class="channel--content-first__more">
            <?php
            // Third Side Article Loop ($module_life_loop_three)
            if ($module_life_loop_three->have_posts()) :
                while ($module_life_loop_three->have_posts()) :
                    $module_life_loop_three->the_post(); ?> 

                <article class="channel--article-style channel--content-first__more__article">
                <figure>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_post_thumbnail('large'); ?>
                    </a>
                </figure>

                <div class="channel--content-first__more__article_info">
                    <h3 class="channel--article-style_title channel--article-style_title__small">                
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    <div class="channel--article-style__metas">
                    <span>por <i><?php echo get_the_author_meta('display_name'); ?></i></span>
                    </div>
                </div>
                
                </article>

                <?php endwhile; endif;
            wp_reset_postdata(); 
            // \\End Third Article Loop ($module_life_loop_three)
            ?>
            </div>
        </div>

        <div class="channel--content-second">
            <div class="channel--sticky">
        <?php
        // Second Side Article Loop ($module_life_loop_two)
        if ($module_life_loop_two->have_posts()) :
            while ($module_life_loop_two->have_posts()) :
                $module_life_loop_two->the_post(); ?> 
        <article class="channel--article-style channel--content-second__articles">
            <figure>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail('large'); ?>
                </a>
            </figure>
            <h3 class="channel--article-style_title channel--article-style_title__medium">                
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_title(); ?>
                </a>
            </h3>
                <div class="channel--article-style__metas">
                <span>por <i><?php the_author_meta('display_name'); ?></i></span>
                </div>
        </article>
        <?php endwhile; endif;
            wp_reset_postdata(); 
            // \\End Second Article Loop ($module_life_loop_two)
            ?>
        </div>
        </div>
    </div>

</section>


		<!--pinno-home-grid-->

		<?php

		/* After widget (defined by themes). */
		echo $after_widget;

	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['tagcat'] = strip_tags( $new_instance['tagcat'] );
		$instance['enterslug'] = strip_tags( $new_instance['enterslug'] );
		$instance['sidebarpos'] = strip_tags( $new_instance['sidebarpos'] );
		$instance['code'] = $new_instance['code'];

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Title' );
        $instance = wp_parse_args( (array) $instance, $defaults ); 
        ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
		</p>

		<!-- Cat/Tag -->
		<p>
			<label for="<?php echo $this->get_field_id('tagcat'); ?>">Display Posts By Category Or Tag:</label>
			<select id="<?php echo $this->get_field_id('tagcat'); ?>" name="<?php echo $this->get_field_name('tagcat'); ?>" style="width:100%;">
				<option value='category' <?php if ('category' == $instance['tagcat']) echo 'selected="selected"'; ?>>Category</option>
				<option value='tag' <?php if ('tag' == $instance['tagcat']) echo 'selected="selected"'; ?>>Tag</option>
			</select>
		</p>

		<!-- Enter Cat/Tag -->
		<p>
			<label for="<?php echo $this->get_field_id( 'enterslug' ); ?>">Enter Category/Tag Slug Name:</label>
            <input id="<?php echo $this->get_field_id( 'enterslug' ); ?>" name="<?php echo $this->get_field_name( 'enterslug' ); ?>" value="<?php echo $instance['enterslug']; ?>" style="width:90%;" />
        </p>

		<!-- Sidebar Position -->
		<p>
			<label for="<?php echo $this->get_field_id('sidebarpos'); ?>">Position of Sidebar Posts:</label>
			<select id="<?php echo $this->get_field_id('sidebarpos'); ?>" name="<?php echo $this->get_field_name('sidebarpos'); ?>" style="width:100%;">
				<option value='left' <?php if ('left' == $instance['sidebarpos']) echo 'selected="selected"'; ?>>Left</option>
				<option value='right' <?php if ('right' == $instance['sidebarpos']) echo 'selected="selected"'; ?>>Right</option>
			</select>
		</p>

		<!-- Ad code -->
		<p>
			<label for="<?php echo $this->get_field_id( 'code' ); ?>">Optional 300x250 Ad Code:</label>
			<textarea id="<?php echo $this->get_field_id( 'code' ); ?>" name="<?php echo $this->get_field_name( 'code' ); ?>" style="width:96%;" rows="6"><?php echo !empty($instance['code']) ? $instance['code'] : ''; ?></textarea>
		</p>

	<?php
	}
}