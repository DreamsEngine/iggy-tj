<?php
/**
 * Plugin Name: Home Main Widget
*/

add_action('widgets_init', 'pinno_load_home_main_widget');

function pinno_load_home_main_widget() {
  register_widget('pinno_home_main_widget');
}

class pinno_home_main_widget extends WP_Widget { 
   
    // class constructor
	public function __construct() {
      $widget_ops = array( 
        'classname' => 'pinno_home_main_widget',
        'description' => 'A widget that displays the main post and this related posts',
      );
      parent::__construct( 'pinno_home_main_widget', ' IT0 - Home Main Widget', $widget_ops);
    }
	
	// output the widget content on the front-end
	public function widget( $args, $instance ) { ?>

	<?php 
	  $first_post = get_first_post(); 
	  $layout_type = ($first_post != NULL ? $first_post->layout_type : "");
	?>

	<?php if ($layout_type == 'front-2') { ?>
	  <section id="module-2" class="f4_module f4_module--one cf f4_module--one--type2">
        <!-- Wrapper --> 
		<div class="f4_module-wrap cf f4_warpper">
            <!-- Article -->
            <article class="f4_module--one-article--second cf" data-article="article-post-<?php $first_post->ID ?>" data-featured="article-1">
                <?php
                  $takeUrl = rwmb_meta('den_layout_new_url','url');
                  $redirectUrl = $takeUrl != null ? $takeUrl : $first_post->post_permalink;
                ?>
                
				<!-- Image Cover -->
                <a href="<?php echo $redirectUrl;  ?>" title="<?php echo $first_post->post_title ?>">
                  <figure class="f4_module--one-article--image--second f4_module-resize--third">
                    <?php echo $first_post->post_thumbnail640 ?>
                  </figure>
                </a>
                <!-- End Image Cover -->
                
                <!-- Meta Information -->
                <div class="f4_module--one-article--right--second" data-post-id="<?php $first_post->ID ?>">
                   <!-- Title -->
                    <h1 class="f4_title"><a href="<?php echo $redirectUrl;  ?>" title="<?php echo $first_post->post_title ?>"><?php echo $first_post->post_title ?></a></h1>
                    <!-- Description -->
                    <p class="f4_module--one-article--right--second__except"><?php echo $first_post->excerpt ?></p>
                    <!-- Autor, Time, Category -->
                    <div class="f4_module--one-metas--second f4_metas">
                        <span>por <?php echo $first_post->author_posts_link ?> | hace <?php echo $first_post->time_ago ?></span>
                        <span>en <?php echo $first_post->category_link ?></span>
                    </div>

                    <?php

                    $related_posts = get_related_post_by_post_conector($first_post->ID);

                    if (count($related_posts) > 0) : ?>
                        <!-- Related Post -->
                        <div class="f4_module--one-related--second">
                            <hr>
                            <h2 class="f4_title">Noticias Relacionadas</h2>
                            <!-- List Related -->
                            <ul class="f4_module--one-releated-list--second">
                                <?php
                                foreach ($related_posts as $post) : setup_postdata($post); ?>
                                    <li class="f4_module--one-related-list-item" data-post-id="<?php $post->ID; ?>">
                                        <a href="<?php echo get_permalink( $post->ID ); ?>"
                                           title="<?php echo $post->post_title; ?>"> <?php echo $post->post_title; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul> <!-- End List Related -->
                        </div> <!-- End Related Post -->
                    <?php endif  //End Tags If ?>
                </div> <!-- End Meta Information -->
            </article>
            <!-- End Article -->
        </div> 
		<!-- End Wrapper -->
      </section>
	<?php } ?> 
    
	<?php if ($layout_type == 'front-4') { ?>
      <!-- Module One Type 4 -->
      <section id="module-4" class="f4_module f4_module--one f4_module--one--type4">
        <!-- Wrapper -->
        <div class="f4_module-wrap cf f4_warpper">
            <div class="articles-list--type4">


                <article>
                    <figure>
                        <?php echo $first_post->post_thumbnail640 ?>
                    </figure>
                    <div class="articles-list--type4__meta">
                        <h2 class="f4_title">
                            <a href="<?php echo $first_post->post_permalink ?>" title="<?php echo $first_post->post_title ?>">
                                <?php echo $first_post->post_title ?>
                            </a>
                        </h2>
                        <span class="f4_metas">Por <?php echo $first_post->author_posts_link ?></span>
                        <span class="f4_metas"><?php the_date() ?></span>
                        <span class="f4_metas">En <?php echo $first_post->category_link  ?></span>
                    </div>
                </article>




                <?php
                // Query
                
                $related_posts = get_related_post_by_post_conector($first_post->ID);
                
                if (count($related_posts) > 0) :
                    foreach ($related_posts as $post) : setup_postdata($post); ?>
                       
                        <article>
                            <figure>
                                <?php the_post_thumbnail('dreams-640x360', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover')); ?>
                            </figure>
                            <div class="articles-list--type4__meta">
                                <h2 class="f4_title">
                                    <a href="<?php echo get_permalink( $post->ID ); ?>" title="<?php echo $post->post_title; ?>">
                                        <?php echo $post->post_title; ?>
                                    </a>
                                </h2>
                                <span class="f4_metas">Por <?php the_author_posts_link(); ?></span>
                                <span class="f4_metas"><?php the_date() ?></span>
                                <span class="f4_metas">En <?php category_link(); ?></span>
                            </div>
                        </article>

                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="no_related_post">No existen post relacionados aun.</div>
                    <?php
                endif; ?>


            </div>
        </div>
        <!-- End Wrapper -->
        <div class="articles-list--type4--mobile">
            <ul class="articles-list--type4--mobile__carousel owl-moduleone-type4 owl-carousel owl-theme f4_footbar__interest__lists f4_footbar__interest__lists--carousel">

                <li>
                    <figure>
                        <a href="<?php echo $first_post->post_permalink ?>" title="<?php echo $first_post->post_title ?>">
                            <?php echo $first_post->post_thumbnail640; ?>
                        </a>
                    </figure>
                    <div class="articles-list--type4--mobile__metas">
                   <span class="f4_metas">
                       <?php echo $first_post->category_link; ?>
                   </span>
                        <a href="<?php echo $first_post->post_permalink ?>" title="<?php echo $first_post->post_title ?>">
                            <h4 class="f4_title"><?php echo $first_post->post_title ?></h4>
                        </a>
                    </div>
                </li>


                <?php
                // Query
                $related_posts = get_related_post_by_post_conector($first_post->ID);

                if (count($related_posts) > 0) :
                    foreach ($related_posts as $post) : setup_postdata($post); ?>

                        <li>
                            <figure>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php the_post_thumbnail('dreams-640x360', array('class' => 'f4_module-image--full lazyload', 'data-object-fit' => 'cover')); ?>
                                </a>
                            </figure>
                            <div class="articles-list--type4--mobile__metas">
                   <span class="f4_metas">
                       <?php category_link(); ?>
                   </span>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <h4 class="f4_title"><?php the_title(); ?></h4>
                                </a>
                            </div>
                        </li>

                    <?php endforeach; endif; ?>



            </ul>
        </div>
      </section>
      <!-- End Module One Type 4 -->   
	<?php } ?>

	<?php if ($layout_type == 'front-5') { ?>
	  <?php
        $background_styles = "";
        $background_styles = "background-image: url('$first_post->image_background');";
        $background_styles .= ($first_post->layout_fixed_background === "fixed" ? "background-attachment: fixed" : "");
        $streaming_link = rwmb_meta('den_layout_links_go','url');
      ?>

      <!-- Module One Type 5 -->
      <section id="module-5" class="f4_module--one f4_module--one--type5" style="<?php echo $background_styles; ?> background-repeat:no-repeat; background-position:center center;">
        <!-- Wrapper -->
        <div class="f4_module-wrap cf f4_warpper">
            <article>
                <div>
                    <figure class="f4_module-resize--third" id="module-f5__figure" data-video="<?php echo $first_post->video_url; ?>">
                        <?php echo $first_post->post_thumbnail640 ?>
                        <div class="f4_module--one--type5__cover">
                            <button class="play-button" type="button" onclick="showModuleOneType5Video();"></button>
                        </div>
                    </figure>
                </div>
            </article>

            <div class="f4_module--one--type5__logo">
                <a href="<?php echo $streaming_link; ?>" target="_blank">
                <figure class="f4_module-resize--third">
                    <img src="<?php echo $first_post->image_logos; ?>" class="f4_module-image--full lazyload" alt="logo" data-object-fit="cover">
                </figure>
                </a>
            </div>
        </div>
        <!-- End Wrapper -->

      </section>
      <!-- End Module One Type 5 --> 
	<?php } ?>
      
<?php }
  // output the option form field in admin Widgets screen
 
	// save options
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['vidgall'] = strip_tags( $new_instance['vidgall'] );

		return $instance;
	}

	public function form( $instance ) {
   
		/* Set up some default widget settings. */
		 $defaults = array( 'title' => 'Title' );
		 $instance = wp_parse_args( (array) $instance, $defaults ); ?>
	 
		 <!-- Widget Title: Text Input -->
		 <p>
		   <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
		   <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
		 </p>
		   
	 <?php }
   
} 



