import Glide from '@glidejs/glide';

const VideoGlider = () => {
  const videoglide =  document.getElementById("videoGal");
  const arrowRight =  document.querySelector(".arrow--right");
  const arrowLeft =  document.querySelector(".arrow--left");
  const sectionThree = document.getElementById('bg-sect');  
  const activeSlide = document.getElementsByClassName('glide__slide video_list_preview glide__slide--active');
  /* const activeImg = activeSlide[0].querySelector('img');
  const activeImgSrc = activeImg.getAttribute('src');  */
  
  const changeBg = () => {
    sectionThree.style.backgroundImage = `url("${activeSlide[0].children[0].children[0].src}")`;
  };

  if(videoglide){
    const glide = new Glide('#videoGal', {
        type: 'carousel',
        startAt: 0,
        perView: 3,
        focusAt: 'center',
        breakpoints: {
          920: {
            perView: 2,
            peek: {
              before: 0,
              after: 50
            }
          },
          480: {
            perView: 1,
            peek: {
              before: 0,
              after: 50
            }
          }
        }
    });
    
    arrowRight.addEventListener("click",()=>{
      glide.go('>');
    });

    arrowLeft.addEventListener("click",()=>{
      glide.go('<');
    });

    glide.on('run.after', () => {
      changeBg();
    });
    
    
    

    glide.mount();  
    
  }
};
  
  export default VideoGlider;

  // videos


